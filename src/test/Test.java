package test;

import fileImport.readers.*;
import main.Main;
import main.Part;

import javax.swing.*;
import java.awt.*;

public class Test {
    private static JFrame mainFrame;
    private static JLabel headerLabel;
    private static JLabel statusLabel;
    private static JPanel controlPanel;
    private static JLabel msglabel;

    public static void main(String[] args){
        Part newPart = new Part( "01-TEST", "hello" );
        Part secondPart = new Part( "01-TEST", "hello" );
        newPart.setVendor( "Hi inc." );
        secondPart.setVendor( "Gutentag inc." );

        Main.getDatabase().addPart( newPart );
        Main.getDatabase().addPart( secondPart );

        
    }
}
