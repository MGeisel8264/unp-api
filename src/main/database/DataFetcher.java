package database;

import java.sql.*;
import java.util.*;
import general.*;

/**
 * Talks to the database
 */
class DataFetcher {
    
    private Statement stmt;
    private Connection c;

    /**
     * Default constructor
     * @throws RuntimeException if opening the database fails
     */
    public DataFetcher(){
        c = null;

        stmt = null;
    
        try{
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:Z:\\SQLite\\sqldb.db");            //Opens document

            stmt = c.createStatement();
        }catch( ClassNotFoundException e ){
            throw new RuntimeException( e.getMessage() );                                   //Throws runtime exception if error
        }catch( SQLException e ){
            throw new RuntimeException( e.getMessage() );                                   //Throws runtime exception if error
        }
    }

    public synchronized boolean addPart( Part part, String table ){
        
        String readName = "INSERT INTO " + table + " (UPN, DESCRIPTION, DESC2, VENDOR, VPN, INITIALS_DATE, USED_ON )" + 
        " values ('" + part.getUPN() + "', '"+ part.getDescription() + "', '"+ part.getDescription2() + "', '"+ 
        part.getVendor() + "', '"+ part.getVPN() + "', '"+ part.getInitialsDate() + "', '" + part.getUsedOn() + "')";
        
        //String readName = "INSERT INTO TABLE02 (UPN, DESCRIPTION) values ('HELLO', 'TEST') ";
        try{
            stmt.execute( readName );

        }catch( Exception e ){
            //This runs if the part is not unique( usually )
            return false;
        }
        return true;
    }   

    public synchronized boolean includes( String table, String column, String query ){
        try{
            ResultSet rs = stmt.executeQuery( "SELECT * FROM TABLE" + table + " WHERE "
             + column + " = '" + query + "';" );
            if( rs.next() ){
                return true;
            }else{
                return false;
            }
        }catch( SQLException e ){
            return false;
        }
        
    }

    public synchronized ArrayList<Part> getPartsList( String table ) throws SQLException{
        try{

            ArrayList<Part> returnParts = new ArrayList<Part>();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM " + table + " ORDER BY UPN;" );
         
            while ( rs.next() ) {
                
                String upn = ""; String description = "";
                try{
                    upn = rs.getString( "UPN" ); description =  rs.getString( "DESCRIPTION" );
                    Part currentPart = new Part( upn, description );
                    currentPart.setDescription2( rs.getString( "DESC2" )); currentPart.setVendor( rs.getString( "VENDOR" ) );
                    currentPart.setVPN( rs.getString( "VPN" ) ); currentPart.setInitialsDate( rs.getString( "INITIALS_DATE" ));
                    currentPart.setUsedOn( rs.getString( "USED_ON" ));
                    returnParts.add( currentPart );
                }catch( IllegalArgumentException e ){
                    stmt.execute( "DELETE FROM " + table + " WHERE UPN = '" + upn + "';" );
                }

            }

         return returnParts;
            
        }catch( Exception e ){
            throw e;
        }
    }

    public synchronized void deleteTable( String table ) throws SQLException{
        stmt.execute( "DELETE FROM " + table + ";" );
    }

    public synchronized void deletePart( String table, String upn ) throws SQLException{
        stmt.execute( "DELETE FROM " + table + " WHERE UPN LIKE '" + upn + "';" );
    }

    public synchronized Part getPart( String table, String upn ) throws SQLException{
        ResultSet rs = stmt.executeQuery( "SELECT * FROM " + table + " WHERE UPN = '" + upn + "';" );
        String description =  rs.getString( "DESCRIPTION" );
        Part currentPart = new Part( upn, description );
        currentPart.setDescription2( rs.getString( "DESC2" )); currentPart.setVendor( rs.getString( "VENDOR" ) );
        currentPart.setVPN( rs.getString( "VPN" ) ); currentPart.setInitialsDate( rs.getString( "INITIALS_DATE" ));
        currentPart.setUsedOn( rs.getString( "USED_ON" ));
        
        return currentPart;
    }

    public synchronized void close(){
        try{
            stmt.close();
            c.close();
        }catch( Exception e ){

        }
    }
}
