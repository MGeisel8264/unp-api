package database;

import general.*;
import java.util.*;
import java.sql.*;

public class LocalDatabase{
    
    private DataFetcher fetcher = new DataFetcher();
    private ArrayList<ArrayList<Part>> partsList = new ArrayList<ArrayList<Part>>();

    public LocalDatabase(){
        Thread initThread = new Thread(){
           public void run(){
               partsList.add( new ArrayList<Part>() );
               for( int i = 1; i < 101; i++ ){
                   try{
                    partsList.add( fetcher.getPartsList( getTable(i) ) );
                   }catch( Exception e ){
                       System.out.println( "ERROR ON ITERATION " + i );
                   }
               }
           }
        };
        initThread.start();
    }

    public int size(){ return partsList.size(); }

    public static String getTable( int i ){
        return "TABLE" + ( (i<10)? "0" + i : i );
    }

    public static String getTable( String upn ){
        try{
            int i = Integer.parseInt( upn.substring( 0, 2 ) );
            return "TABLE" + ( (i<10)? "0" + i : i );
        }catch( Exception e ){
            return "TABLE100";
        }
        
        
    }

    public void addPart( Part addPart ){
        //This isn't getting called. I will implement this differently( this is a slower way of doing it anyway )

        if( partsList.get( addPart.getPrefix()).contains( addPart ) ){
            return;
        }

        for( Part currentPart : partsList.get( addPart.getPrefix()) ){
            if( currentPart.getUPN().equals( addPart.getUPN() ) ){
                
                partsList.get( addPart.getPrefix()).remove( currentPart );
                String[] addPartArr = addPart.getAsArray();
                for( int j = 0; j < addPartArr.length; j++ ){
                    if( addPartArr[j].contains( currentPart.getAsArray()[j] ) ){
                        continue;
                    }else if( addPartArr[j].equals( "" ) || currentPart.getAsArray()[j].equals( "" ) ){
                        addPartArr[j] = addPartArr[j] + currentPart.getAsArray()[j];
                    }else{
                        addPartArr[j] = addPartArr[j] + "; " + currentPart.getAsArray()[j];
                    }
                }

                try{
                    fetcher.deletePart( getTable( addPart.getPrefix() ), currentPart.getUPN() );
                }catch( Exception e ){ e.printStackTrace(); }
                addPart.setAsArray( addPartArr );
                break;
            }
        }
        
        partsList.get( addPart.getPrefix() ).add( addPart );
        fetcher.addPart( addPart, getTable( addPart.getPrefix() ) );
    }

    public void sort(){
        Comparator<Part> comparator = new Comparator<Part>(){
            
            public int compare( Part object1, Part object2) {
                return ( object1.getUPN().compareTo(object2.getUPN())  );
            }
        };
        
        for( int i = 0; i < partsList.size(); i++ ){
            partsList.get(i).sort( comparator );
        }
    }

    public void deletePart( Part part){
        try{
            fetcher.deletePart( getTable( part.getUPN() ), part.getUPN() );
        }catch( SQLException e ){
            throw new RuntimeException( e );
        }
    }

    public void deleteTable( int table ){
        partsList.remove( table );
        partsList.add( table,  new ArrayList<Part>() );
        Thread thisThread = new Thread(){
            public void run(){
                try{
                    fetcher.deleteTable( "TABLE" + ( (table<10)? "0" + table : table ) );
                }catch( Exception e ){
                    throw new RuntimeException( e );
                }
            }
        };
        thisThread.start();
    }

    //ACCESSOR METHODS
    public ArrayList<Part> getList( int index ){
        return partsList.get( index );
    }

    public ArrayList<Part> getList( int index, boolean showObs ){
        ArrayList<Part> returnList = new ArrayList<Part>();
        for( int i = 0; i < partsList.get( index ).size(); i++ ){
            if( !partsList.get( index ).get(i).isObsolete() || showObs ){
                returnList.add( partsList.get( index ).get(i) );
            }
        }

        return returnList;
    }

    public boolean includes( int index, String upnString ){
        for( int i = 0; i < partsList.get( index ).size(); i++ ){
            if( partsList.get( index ).get( i ).getUPN().equals( upnString ) ){
                return true;
            }
        }
        return false;
    }

}
