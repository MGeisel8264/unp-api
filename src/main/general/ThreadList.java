package general;

import java.util.ArrayList;

/**
* This is a list of all file reader classes
*/
public class ThreadList<T extends Thread> extends ArrayList<T>{
    
    //Stores if the threads are alive
    private ArrayList<Boolean> threadsStarted = new ArrayList<Boolean>();
    //determines whether threads are still being added
    public boolean threadsAdded = false;

    /**
     * Default constructor
     */
    public ThreadList(){
    }

    /**
     * Adds thread to list
     */
    public void startThread( T addThread ){
        add( addThread );
        threadsStarted.add( false );
        addThread.start();
    }

    /**
     * @return number of threads running
     */
    public int getNumbThreadsRunning(){
        return getRunningThreads().size();
    }

    /**
     * 
     * @return number of threads completed
     */
    public int getNumbThreadsCompleted(){
        return size();
    }

    /**
     * @return all threads currently running
     */
    private ThreadList<T> getRunningThreads(){
        ThreadList<T> runningThreads = new ThreadList<T>();
        for( int i = 0; i < size(); i++ ){
            //Checks if thread is alive
            if( get( i ).isAlive() ){
                runningThreads.add( get( i ) );
            }
        }
        return runningThreads;
    }
}

