package general;
import java.awt.event.*;

import javax.swing.JPanel;

public abstract class BtnActionListener implements ActionListener{

    protected JPanel thisPanel = new JPanel();
    
    public abstract void actionPerformed( ActionEvent e );

    protected class CancelListener implements ActionListener{
        @Override
        public void actionPerformed( ActionEvent e ){
            thisPanel.setVisible( false );
            Main.getPanel().setVisible( true );
        }
    }
    
}
