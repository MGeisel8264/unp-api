package general;

public class Part {

    //Variables
    private String upn;
    private String description;
    private String description2 = "";
    private String vendor = "";
    private String vpn = "";
    private String initialsDate = "";
    private String usedOn = "";

    //Default Constructor
    public Part( String upn, String description ){
        setUPN( upn );
        this.description = description;
    }

    //ACCESSOR METHODS
    public String getUPN(){ return upn; }
    public String getDescription(){ return description; }
    public String getDescription2(){ return description2; }
    public String getVendor(){ return vendor; }
    public String getVPN(){ return vpn; }
    public String getInitialsDate(){ return initialsDate; }
    public String getUsedOn(){ return usedOn; }

    /**
     * @return prefix of the part
     */
    public int getPrefix(){
        try{
            return Integer.parseInt( upn.substring( 0, 2 ) );
        }catch( Exception e ){
            return 100;
        }
    }

    /**
     * @return prefix of string upn
     */
    public static int getPrefixOf( String upn ){
        try{
            return Integer.parseInt( upn.substring( 0, 2 ) );
        }catch( Exception e ){
            return 100;
        }  
    }

    public boolean isObsolete(){
        if( description.contains( "*OBSOLETE*" ) || description.contains( "*OBS*" )
        || description2.contains( "*OBSOLETE*" ) || description2.contains( "*OBS*" ) ){
            return true;
        }

        return false;
    }

    /**
     * Special accessor method- returns values as a string 
     * Upn, description, description 2, vendor, vpn, initials & date, used on
     */
    public String[] getAsArray(){
        String[] returnString = { getUPN(), getDescription(), getDescription2(), 
            getVendor(), getVPN(), getInitialsDate(), getUsedOn() };
        return returnString;
    }

    //MUTATOR METHODS
    public void setUPN( String input ){ upn = input; }
    public void setDescription( String desc ){ this.description = desc; }
    public void setDescription2( String desc2 ){ this.description2 = desc2; }
    public void setVendor( String vendor ){ this.vendor = vendor; }
    public void setVPN( String vpn ){ this.vpn = vpn; }
    public void setInitialsDate(String ind ){ initialsDate = ind; }
    public void setUsedOn( String usedOn ){ this.usedOn = usedOn; }

    /**
     * Special mutator method- edits values as a string
     * Upn, description, description 2, vendor, vpn, initials & date, used on
     */
    public void setAsArray( String[] input ){
        setUPN( input[0] ); setDescription( input[1] );setDescription2( input[2] ); setVendor( input[3] );
        setVPN( input[4] ); setInitialsDate( input[5] ); setUsedOn( input[6] );
    }
}
