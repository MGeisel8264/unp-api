package general;

import javax.swing.*;
import java.awt.*;

public abstract class UPNMenu extends JPanel{

    //TODO: Document this class

    protected String topText;
    protected String bottomBtnText;

    protected UPNMenu( String topText, String bottomBtnText ){
        super( new BorderLayout() );

        this.topText = topText;
        this.bottomBtnText = bottomBtnText;
    }

    public final void main(){
        initialize();
        add( Main.getTopPanel( topText ), BorderLayout.NORTH );
        centerPanel();
        bottomPanel();
        Main.getFrame().add( this );
    }

    private void initialize(){
        try{
            Main.getPanel().setVisible( false );
            setBackground( Color.BLACK );                           //sets background
            setVisible( true );
        }catch( Exception ex ){
            JOptionPane.showMessageDialog( null, "ERROR: " + ex.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE );     //shows error if fails
        }
    }

    protected class TxtLabel extends JLabel{
        public TxtLabel( String text ){
            super( text, SwingConstants.CENTER );
            setForeground( Color.WHITE );
        }
    }

    protected class PartCategories extends JComboBox<String>{
        public PartCategories(){
            String[] comboStrs = new String[100];                                   //combo box strings
            for( int i = 0; i < 99; i++ ){
                if( i + 1 < 10 ){
                    comboStrs[i] = "0" + Integer.toString( i + 1 );
                }else{
                    comboStrs[i] = Integer.toString( i + 1 );
                }
                addItem( comboStrs[i] );
            }
            comboStrs[99] = "SPECIAL";
            addItem( comboStrs[99] );
        }
    }

    protected abstract void centerPanel();

    protected void bottomPanel(){
        FlowLayout flowLayout = new FlowLayout(); flowLayout.setHgap(75);
        JPanel bottomPanel = new JPanel( flowLayout ); bottomPanel.setBackground( new Color( 6, 24, 54) );
        JButton customButton = new JButton( bottomBtnText );
        JButton cancelButton = new JButton( "CANCEL" );
        bottomPanel.add( customButton ); bottomPanel.add( cancelButton );
        add( bottomPanel, BorderLayout.SOUTH );

        cancelButton.addActionListener( e -> cancelButton() );
        customButton.addActionListener( e -> bottomButton() );
    }

    protected void cancelButton(){
        setVisible( false );
        Main.main( new String[1] );
    }

    protected void closePanel(){
        setVisible( false );
    }

    protected abstract void bottomButton();

}