/**
 * This class contains everything that all classes will need to access( like main panel, main database, etc. )
 */
package general;

import javax.swing.*;

import add.Add;
import frontend.Import;
import database.LocalDatabase;
import query.Query;

import java.awt.*;
import java.awt.event.*;

public final class Main{

    //Constants galore
    private static final String APP_NAME = "Unilux Part Database";
    private static final String ICON_NAME = "Resources\\Logo.png";
    public static final String[] ATTRIBUTE_NAMES = { "Unilux Part Numb.", "Description", "Desc. 2", "Vendor Name", "Vendor Part Numb.",
    "Initials & Date", "Used On" };

    //Variables & Objects used by everyone
    private static JFrame frame = new JFrame( APP_NAME );
    private static JPanel panel;
    private static Toolkit toolkit = Toolkit.getDefaultToolkit();
    //TODO: Create Internal Part list
    private static LocalDatabase database;

    //Accessor Methods
    public static JFrame getFrame(){ return frame; }
    public static JPanel getPanel(){ return panel; }
    public static LocalDatabase getDatabase(){ return database; }
    public static Toolkit getToolkit(){ return toolkit; }

    //Mutator Methods
    public static void setFrame( JFrame frame ){ Main.frame = frame; }
    public static void setPanel( JPanel panel ){ Main.panel = panel; }

    public static void main( String[] args ){

        database = new LocalDatabase();

        //ATTEMPTS TO SET THE LOOK & FEEL
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception ex) { 
            System.out.println( "ERROR" );
            JOptionPane.showMessageDialog( null, "ERROR: " + ex.getMessage(), 
            "Error Message", JOptionPane.ERROR_MESSAGE );            //error message
            throw new RuntimeException( ex.getMessage() );
        }

        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );                     //ensures the window will exit
        Image icon = toolkit.getImage( ICON_NAME );                                 //makes the app icon

        frame.setBackground( Color.BLACK );                                         //sets background color
        frame.setIconImage( icon );                                                 //sets icon image 
        frame.setSize( 500, 500 );                                                  //sets window size
        panel = new JPanel( new BorderLayout() );                                   //instantiates panel
        panel.setBackground( Color.BLACK );                                         //sets panel background

        frontPanel();

        frame.add( panel );
        frame.setVisible( true );        
    }

    /**
     * This method returns the image at location name in the form of a JLabel
     */
    public static JLabel getImage( String name, int width, int height ){
        Image img = toolkit.getImage( name );
        img = img.getScaledInstance( width, height, 1);
        ImageIcon i = new ImageIcon( img );
        return new JLabel( i );
    }

    /**
     * @param text the text in the panel
     * @return a formatted panel with text and the Unilux logo
     */
    public static JPanel getTopPanel( String text ){
        JLabel bufferLabel = new JLabel( "                          " );

        //creates new JPanel and sets it to be black
        JPanel topPanel = new JPanel( new FlowLayout() );
        topPanel.setBackground( Color.BLACK );

        //adds the Unilux logo to the top frame
        topPanel.add( getImage( ICON_NAME, 50, 50 ) );

        //adds buffer
        topPanel.add( bufferLabel );

        //Generates the main text and formats it
        JLabel mainText = new JLabel( text, SwingConstants.CENTER ); topPanel.add( mainText );
        mainText.setForeground( Color.WHITE ); Font font = new Font("Courier", Font.BOLD,24); mainText.setFont( font );

        return topPanel;
    }

    /**
     * Generates the front panel
     * This is a MAIN METHOD
     */
    private static void frontPanel(){
        //----------------------------------------------
        //--------------TOP PANEL-----------------------
        //----------------------------------------------

        //Generates main panel
        JPanel topPanel = new JPanel( new BorderLayout() );
        topPanel.setBackground( Color.BLACK );

        //Creates the main text in the Menu screen
        JLabel mainText = new JLabel( "Unilux Part Number Query", SwingConstants.CENTER ); topPanel.add( mainText, BorderLayout.NORTH ); 
        mainText.setForeground( Color.WHITE ); Font font = new Font("Courier", Font.BOLD,36); mainText.setFont( font );
       
        topPanel.add( getImage( ICON_NAME, 100, 100 ), BorderLayout.CENTER );                       //generates main Unilux image
        panel.add( topPanel, BorderLayout.NORTH );                                                  //add the top panel to the main panel


        /*----------------------------------------------
        ---------------MIDDLE PANEL---------------------
        ----------------------------------------------*/

        GridLayout mainLayout = new GridLayout( 2, 3 );                                             //creates new layout
        mainLayout.setHgap( 20 );                                                                   //sets horizontal border
        JPanel mainPanel = new JPanel( mainLayout );                                                //generates main panel
        mainPanel.setBorder( BorderFactory.createEmptyBorder( 20, 20, 20, 20 ) );                   //creates border
        panel.add( mainPanel, BorderLayout.CENTER );                                                //adds the middle panel
        
        mainPanel.setBackground( Color.BLACK );                                                     //sets background

        //-----LABEL CREATION-----

        mainText = new JLabel( "ADD NEW PART", SwingConstants.CENTER );                             //Add new part label
        mainText.setForeground( Color.WHITE );
        mainPanel.add( mainText );
        mainText = new JLabel( "INSERT FILE", SwingConstants.CENTER );                              //Insert file label
        mainText.setForeground( Color.WHITE );
        mainPanel.add( mainText );
        mainText = new JLabel( "QUERY PARTS", SwingConstants.CENTER );                              //Query parts label
        mainText.setForeground( Color.WHITE );
        mainPanel.add( mainText );

        //-----BUTTON CREATION-----

        //NEW PART BUTTON
        JButton newPartBtn = new JButton( "SELECT" );                                                   
        newPartBtn.addActionListener( new BtnActionListener(){
            @Override
            public void actionPerformed( ActionEvent e ){
                Add currentAddPanel = new Add();
                currentAddPanel.main();
            }
        });                                                                                         //Creates new part button

        //OPEN FILE BUTTON
        JButton openFileBtn = new JButton( "SELECT" );
        openFileBtn.addActionListener( new BtnActionListener(){
            @Override
            public void actionPerformed( ActionEvent e ){
                Import currentImportPanel = new Import();
                currentImportPanel.main();
            }    
        });                                                                                         //Creates open file button

        //QUERY BUTTON
        JButton queryPartBtn = new JButton( "SELECT" );
        queryPartBtn.addActionListener( new BtnActionListener(){
            @Override
            public void actionPerformed( ActionEvent e ){
                Query currentQueryPanel = new Query();
                currentQueryPanel.main();
            }  
        } );                                                                                        //Creates query button

        //Add buttons
        mainPanel.add( newPartBtn );                                                                //add buttons
        mainPanel.add( openFileBtn );
        mainPanel.add( queryPartBtn );
        
    }

}