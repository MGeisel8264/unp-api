package general;

import javax.swing.*;

public interface PasswordProtected {
    
    public default boolean checkPassword( String text ){
        JPasswordField pwd = new JPasswordField(14);
        JOptionPane.showConfirmDialog(null, pwd, text, JOptionPane.OK_CANCEL_OPTION);

        if( new String( pwd.getPassword() ).equals( "orangeblossoms" ) ){
            return true;
        }else{
            JOptionPane.showMessageDialog( null, "Invalid Password", "Invalid", JOptionPane.WARNING_MESSAGE );
            return false;
        }
    }

}
