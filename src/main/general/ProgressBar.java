package general;

import javax.swing.*;
import java.awt.*;

public class ProgressBar extends Thread{
    private static final int LOGO_NUMBS = 5;
    private String writeString;

    private int percentage;

    public ProgressBar( String inputString ){
        writeString = inputString;
    }

    public void setPercentage( int percentage ){ this.percentage = percentage; }
    public int getPercentage(){ return percentage; }

    @Override
        public void run(){

            int logoNumb = LOGO_NUMBS;
            
            JFrame thisFrame = new JFrame( "Progress Bar" );
            thisFrame.setSize( 324, 200  );
            Image icon = Main.getToolkit().getImage( "Resources\\Logo.png" );
            thisFrame.setIconImage( icon );

            JLabel movingLogo = new JLabel( "", SwingConstants.CENTER );
            Image logoImg = Main.getToolkit().getImage( "Resources\\Logo_"+logoNumb+".png" );
            logoImg = logoImg.getScaledInstance( 50, 50, 1);
            ImageIcon logoIcon = new ImageIcon( logoImg );
            movingLogo.setIcon( logoIcon );
            thisFrame.setBackground( Color.BLACK );

            thisFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
            JPanel newPanel = new JPanel( new GridLayout( 3, 1 ));
            JLabel jLabel = new JLabel( writeString, SwingConstants.CENTER ); jLabel.setForeground( Color.WHITE );
            newPanel.setBackground( Color.BLACK );
            newPanel.add( movingLogo );
            newPanel.add( jLabel );
            thisFrame.add( newPanel );
            thisFrame.setVisible( true );

            JProgressBar pBar = new JProgressBar(); pBar.setStringPainted(true);
            pBar.setForeground(Color.blue);
            pBar.setValue( percentage );
            newPanel.add( pBar );
            
            while( percentage < 100 ){
                logoNumb--;
                if( logoNumb < 1 ){
                    logoNumb = LOGO_NUMBS;
                }

                pBar.setValue( percentage );
                Image newLogoImg = Main.getToolkit().getImage( "Resources\\Logo_" + logoNumb + ".png" );
                ImageIcon newLogoIcon = new ImageIcon( newLogoImg.getScaledInstance( 50, 50, 1) );
                movingLogo.setIcon( newLogoIcon );
                try{
                    sleep(200);
                }catch( Exception e ){
                }

            }
            thisFrame.setVisible( false );
            Main.getFrame().setVisible( true );
        }
    
}
