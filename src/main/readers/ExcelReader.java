package readers;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import general.*;
import database.*;

public class ExcelReader extends FileReader{

    public ExcelReader( String fileName ){
        super( fileName );
        partsList = new ArrayList<ArrayList<Part>>();
        for( int i = 0; i < 101; i++ ){
            partsList.add( new ArrayList<Part>() );
        }
    }

    @Override
    public int getNumberOfPages(){
        try{
            File file = new File( fileName );
            FileInputStream fStream = new FileInputStream( file );
            XSSFWorkbook wb = new XSSFWorkbook( fStream );         //creates workbook
            int sheetNumbs = wb.getNumberOfSheets();                                                    //generates number of sheets
            fStream.close();
            wb.close();                                             //closes workbook
            return sheetNumbs;                                      //returns number
        }catch( IOException e ){ return 0; }
    }

    @Override
    protected ReaderThread getThread( int i ){
        return new ExcelThread( i );
    }

    private class ExcelThread extends ReaderThread{

        private XSSFSheet sheet;
        private XSSFWorkbook wb;
        /**
         * Default constructor
         */
        public ExcelThread( int pageNumb ){
            super( pageNumb );
            String sheetName = ( pageNumb >= 10 )? Integer.toString( pageNumb ) : "0" + Integer.toString( pageNumb );
            try{
                wb = new XSSFWorkbook( new FileInputStream( new File( fileName )  ) );
                sheet = wb.getSheet( sheetName );
                wb.close();
            }catch( IOException e ){}
            
        }

        /**
         * Overriden generate next part
         */
        @Override 
        public boolean generateNextPart( int i ){
            try{
                if( i > sheet.getLastRowNum() ){ return false; }    //returns false if we're at the bottom
            }catch( Exception e ){
                return false;
            }
                                                    
            Row currentRow = sheet.getRow( i );   
            try{
                if( currentRow.getCell( 0 ).getStringCellValue().equals( "Part No" ) ){                 //ensures that the first row is not counted
                    return true;
                }
            }catch( NullPointerException e ){
                return false;
            }
            
                
            currentPart = new Part( currentRow.getCell( 0 ).getStringCellValue(), 
            "" );                                          //generates the new part
            try{ currentPart.setDescription(currentRow.getCell( 1 ).getStringCellValue()); }catch( Exception e ){ return false;}
            try{ currentPart.setInitialsDate( currentRow.getCell( 2 ).getStringCellValue()); }catch( Exception e ){}    //sets the initials & date
            try{ currentPart.setUsedOn( currentRow.getCell( 3 ).getStringCellValue() ); }catch( Exception e ){}         //sets the string cell value
                

            addPart();                                                                              //adds the current part
            return true;                                                                            //shows everything is fine
        }

        @Override
        public void initialize(){
    
        }
    }

}
