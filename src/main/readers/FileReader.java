package readers;

import java.util.ArrayList;
import general.*;

public abstract class FileReader {
    /**
     * Determines the type of file being read
     */
    public static enum FileType{ PDF, EXCEL };
    protected FileType thisType;
    //A list that stores all of the parts read by the reader
    protected ArrayList<ArrayList<Part>> partsList = new ArrayList<ArrayList<Part>>();
    //A list that stores all of the threads that are running or abt to run.
    private ThreadList<ReaderThread> threadList = new ThreadList<ReaderThread>();
    //The location of the file
    protected String fileName;
    
    /**
     * @return the partsList of the file
     */
    public ArrayList<ArrayList<Part>> getPartsList(){
        return partsList;
    }

    

    /**
     * Waits until the threads are done
     */
    public void waitForThreads( int waitPeriod, Periodic periodic ){
        System.out.println( "STARTED" );
        threadList.threadsAdded = true;

        Thread addThread = new Thread(){
            @Override
            public void run(){
            for( int i = 0; i < getNumberOfPages(); i++ ){
                threadList.startThread( getThread( i ) );
            }
                threadList.threadsAdded = false;
            }
        };
        addThread.start();

        while( threadList.getNumbThreadsRunning() > 0 || threadList.threadsAdded ){
            periodic.getPercentage( ((double)threadList.getNumbThreadsCompleted() / (double)(getNumberOfPages() - 10 ) )*100 );
            try{
                synchronized( this ){
                    wait( waitPeriod );
                }
                
            }catch( Exception e ){ System.out.println( e.getMessage() ); }
        }

        System.out.println( "DONE" );
    }

    abstract protected ReaderThread getThread( int i );

    /**
     * Waits until the threads are done
     */
    public void waitForThreads( int waitPeriod ){
        waitForThreads( waitPeriod, percentage -> {} );
    }

    public int getRunningThreads(){
        return threadList.getNumbThreadsRunning();
    }

    /**
     * Gives the percentage and allows you to use it
     */
    public interface Periodic{
        public abstract void getPercentage( double percentage );
    }

    /**
     * Constructor: initializes variables( except threadList )
     */
    public FileReader( String fileName ){
        this.fileName = fileName;

        for( int i = 0; i < 100; i++ ){
            partsList.add( new ArrayList<Part>() );
        }
    }

    //Returns number of pages
    public abstract int getNumberOfPages();
   
    /**
     * This is the generic thread for all reader threads
     */
    protected abstract class ReaderThread extends Thread{
        //The current part
        protected Part currentPart = new Part( "", "" );
        //The page number
        protected int pageNumb;

        public ReaderThread( int pageNumb ){
            this.pageNumb = pageNumb;
        }

        /**
         * This method is abstract. It must be overriden.
         * Return false when there are no more parts to generate.
         * The int i will go up every iteration. Use it if you would like.
         */
        public abstract boolean generateNextPart( int i );

        //adds the part to the ArrayList
        public void addPart(){
            partsList.get( currentPart.getPrefix() ).add( currentPart );
            currentPart = new Part( "", "" );
        }

        //This runs first
        public abstract void initialize();

        @Override
        public void run(){
            initialize();
            //continues to generate parts until there are no more
            int i = 0;
             while( generateNextPart( i ) ){ i++; }
        }
    }
}