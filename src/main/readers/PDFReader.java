package readers;
import java.io.IOException;
import java.io.File;
import java.awt.Rectangle;

import general.*;
import java.util.*; 
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripperByArea;

public class PDFReader extends FileReader{

    /**
     * Default constructor
     */
    public PDFReader( String fileName ){
        super( fileName );
        partsList = new ArrayList<ArrayList<Part>>();
        for( int i = 0; i < 101; i++ ){
            partsList.add( new ArrayList<Part>() );
        }
    }

    @Override
    protected ReaderThread getThread( int i ){
        return new PDFThread( i );
    }
    
    /**
     * Returns the number of pages in the PDF
     */
    @Override
    public int getNumberOfPages(){
        try{
            return Loader.loadPDF( new File( fileName ) ).getNumberOfPages();
            
        }catch( Exception e ){
            System.out.println( "ERR- #" );
            return 0;
        }
    }

    public class PDFThread extends ReaderThread{
        //The page that is being worked off of
        private PDPage thisPage;

        //The width of each row
        private final int ROW_WIDTH = 11;

        //The stripper that does all of the work
        private PDFTextStripperByArea stripper;

        //The rectangles for extraction
        
        private Rectangle[] rects = { new Rectangle( 0, 0, 100, 11 ), new Rectangle( 100, 0, 168, 11 ), new Rectangle( 268, 0, 156, 11 ),
            new Rectangle( 424, 0, 175, 11 ), new Rectangle( 599, 0, 200, 11 ) };
        
        /**
         * Default constructor
         */
        public PDFThread( int pageNumb ){
        
            super( pageNumb );  
        }

        /**
         * Sets currentPart to the next part
         */
        @Override
        public boolean generateNextPart( int i ){
            i *= ROW_WIDTH;
            i += ( pageNumb == 0 )? 110 : 45;        
            
            //Adds rows to each rectangle & then creates regions
            for( int j = 0; j < rects.length; j++ ){
                rects[j].y = i;                                 //sets the y-value of the rectangle
                stripper.addRegion( "region" + j, rects[j] );   //adds the region to the stripper
            }
            

            try{
                stripper.extractRegions( thisPage );            //extracts the regions & allows for reading
            }catch( IOException e ){ System.out.println( "ERR" );return false; }


            String upn = stripper.getTextForRegion( "region0" ).trim();
            //Condition for if UPN is empty
            if( upn.equals( "" ) ){   //Checks if the upn slot is empty
                String[] newAttributes = currentPart.getAsArray();              //Will be added to later

                for( int j = 1; j < 5; j++ ){
                    try{
                        newAttributes[j] += stripper.getTextForRegion( "region" + j ).trim();      //adds to the string the next region
                    }catch( NullPointerException e ){
                        newAttributes[j] += "";
                    }
                }

                currentPart.setAsArray( newAttributes );                        //sets the array to the part
            }

            //Condition if we are at the bottom of the document
            else if( upn.contains( ":\\" ) ){
                return false;                                                               //returns false if it finds the :\
            }

            //Condition if there is a new part
            else{

                if( !currentPart.getUPN().equals("") ) {
                    addPart();                                                  //adds the part if the part isnt null
                }
                
                String[] stringList = new String[7];    
                for( int j = 0; j < 7; j++ ){
                    try{
                        stringList[j] = stripper.getTextForRegion( "region" + j ).trim();           //add the next region to the string list
                    }catch( Exception e ){
                        stringList[j] = "";
                    }
                }
               
                currentPart.setAsArray( stringList );                           //sets the string list to the part
            }
            
            return true;                                                        //if nothing went wrong we return true
        }

        @Override
        public void initialize(){
            try{
                //Creates the pdf stripper that will do all of our work
                stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition( true );

                thisPage = Loader.loadPDF( new File( fileName ) ).getPage( pageNumb );
            }catch( Exception e ){
                System.out.println( e.getStackTrace() );
            }
        }

    }
}
