package frontend;

import javax.swing.*;
import javax.swing.plaf.basic.BasicProgressBarUI;
import java.awt.event.*;
import java.awt.*;

import readers.*;

import general.*;

public class Import extends UPNMenu{
    
    private PartCategories jcbox;
    private JTextArea fileNameArea;
    private FileReader reader;
    private JPanel centerPanel;
    private TxtLabel status;

    public Import(){
        super( "File Selection", "IMPORT" );
    }
        
    @Override
    protected void centerPanel(){
        //FILE CHOOSER CODE
        FileChooser chooser = new FileChooser();
        chooser.setListener( str -> readFile( str ) );
        chooser.main();

        //LAYOUT CREATION
        GridLayout layout = new GridLayout( 2, 3 ); layout.setHgap( 10 ); layout.setVgap( 10 );
        GridLayout gLayout = new GridLayout( 3, 2 ); gLayout.setVgap( 10 );
        GridLayout bLayout = new GridLayout( 2, 2 ); bLayout.setHgap( 25 );

        //PANEL CREATION
        centerPanel = new JPanel( new GridLayout( 2, 1 ) );
        JPanel secondPanel = new JPanel( layout ); secondPanel.setBorder( BorderFactory.createEmptyBorder( 10, 20, 10, 20));
            secondPanel.setBackground( Color.BLACK );
        JPanel midPanel = new JPanel( gLayout ); midPanel.setBackground( Color.BLACK );
            midPanel.setBorder( ( BorderFactory.createEmptyBorder( 0, 20, 0, 20)));

        //LABEL CREATION
        TxtLabel statusLbl = new TxtLabel( "FILE STATUS" );
        status = new TxtLabel( "FILE OPEN" ); status.setForeground( Color.GREEN );
        TxtLabel nameLbl = new TxtLabel( "FILE NAME:" );
        TxtLabel newFileLbl = new TxtLabel( "FIND NEW FILE:" );

        TxtLabel resetLabel = new TxtLabel( "RESET TABLE" );
        TxtLabel tableLabel = new TxtLabel( "TABLE" );
        TxtLabel resetAllLabel = new TxtLabel( "RESET ALL" );
        
        //NAME TEXT AREA
        fileNameArea = new JTextArea(); fileNameArea.setForeground( Color.WHITE ); fileNameArea.setBackground( Color.BLACK );
        fileNameArea.setEditable( false ); JScrollPane namePane = new JScrollPane( fileNameArea ); namePane.setBorder( BorderFactory.createEmptyBorder() );

        //BUTTON CREATION
        JButton findNewFile = new JButton( "FIND NEW FILE" );
        JButton resetBtn = new JButton( "SELECT" );
        JButton resetAllBtn = new JButton( "SELECT" );

        //MISC CREATION
        jcbox = new PartCategories();

        //ACTION LISTENERS
        findNewFile.addActionListener( e -> {
            Import thisImport = new Import(); thisImport.main();
        });

        resetBtn.addActionListener( new ResetListener( false ));
        resetAllBtn.addActionListener( new ResetListener( true ));

        //ADD COMPONENTS TO PANELS
        midPanel.add( statusLbl ); midPanel.add( status ); midPanel.add( nameLbl );
        midPanel.add( namePane ); midPanel.add( newFileLbl ); midPanel.add( findNewFile );

        secondPanel.add( resetLabel ); secondPanel.add( tableLabel ); secondPanel.add( resetAllLabel );
        secondPanel.add( resetBtn ); secondPanel.add( jcbox ); secondPanel.add( resetAllBtn );

        centerPanel.add( midPanel );
        centerPanel.add( secondPanel );
        add( centerPanel, BorderLayout.CENTER );

    }
    private class ResetListener implements ActionListener, PasswordProtected{

        boolean resetAll;

        public ResetListener( boolean resetAll ){
            this.resetAll = resetAll;
        }

        @Override
        public void actionPerformed( ActionEvent e ){
            int selection = jcbox.getSelectedIndex() + 1;

            String pWordText = "Enter Password to reset " + ( ( resetAll )? "ALL TABLES" : "table " + selection );
            if( !checkPassword( pWordText) ){
                return;
            }

            if( resetAll ){
                for( int i = 1; i < 101; i++ ){
                    Main.getDatabase().deleteTable( i );
                }
            }else{
                Main.getDatabase().deleteTable( selection );
            }
            System.out.println( Main.getDatabase().size() );
            JOptionPane.showMessageDialog( new JFrame(),"Successful reset.","Success",JOptionPane.INFORMATION_MESSAGE );  
        }
    }

    private void readFile( String fileName ){
        if( fileName.equals( "" ) ){
            status.setText( "FILE NOT OPENED "); status.setForeground( Color.RED );
            reader = new PDFReader( "" ); setVisible( true ); return;
        }
    
        ProgressBar pBar = new ProgressBar( "OPENING FILE..." ); 
            
        if( fileName.substring( fileName.length() - 4, fileName.length() ).equals( ".pdf" ) ){
            reader = new PDFReader( fileName );
        }else{
            reader = new ExcelReader( fileName );
        }
            
        Thread waitThread = new Thread(){
            public void run(){
                reader.waitForThreads( 10, percentage -> pBar.setPercentage((int)percentage)  );
                fileNameArea.setText( fileName );
            }
        };
    
        ThreadDisplay threadDisplay = new ThreadDisplay(){
            public int getValue(){
                return reader.getRunningThreads();
            }
            public boolean keepRunning(){
                return pBar.isAlive();
            }
        };
        waitThread.start();
        pBar.start();
        threadDisplay.start();
    }
    
    @Override
    public void bottomButton(){

        ProgressBar pBar = new ProgressBar( "IMPORTING DATA..." );
        Thread thisThread = new Thread(){ public void run(){
            for( int i = 0; i < reader.getPartsList().size(); i++ ){
                for( int j = 0; j < reader.getPartsList().get(i).size(); j++ ){
                    Main.getDatabase().addPart( reader.getPartsList().get(i).get(j) );
                }
                pBar.setPercentage( pBar.getPercentage() + 1 );
            }

            Main.getDatabase().sort();
            pBar.setPercentage( 100 );
            centerPanel.setVisible( false );
            try{
                wait( 100 );
            }catch( Exception e ){e.printStackTrace();}     
            JOptionPane.showMessageDialog( new JFrame(),"Successful import.","Success",JOptionPane.INFORMATION_MESSAGE );
            cancelButton();
            repaint();
        }};

        
        pBar.start();

        thisThread.start();
        
    }

    private abstract class ThreadDisplay extends Thread{
        @Override
        public void run(){
    
            //CREATED FRAME & PANEL
            JFrame frame = new JFrame( "Thread Display" );
            JPanel panel = new JPanel( new GridLayout( 2, 1 ) );
            frame.setSize( 324, 200  );
    
            panel.setBackground( Color.BLACK );
    
            TxtLabel descLabel = new TxtLabel( "Threads Running" );
            JProgressBar threadsBar = new JProgressBar( 0, 90 );
            threadsBar.setStringPainted( true );
            panel.add( descLabel );
            panel.add( threadsBar );
            frame.add( panel );

            frame.setVisible( true );
            panel.setVisible( true );
    
            threadsBar.setUI(new BasicProgressBarUI() {
                protected Color getSelectionBackground() { return Color.BLACK; }
                protected Color getSelectionForeground() { return Color.BLACK; }
            });
                
            while( keepRunning() ){
                int value = getValue();
                descLabel.setText( "Threads Running: " + value );
                threadsBar.setValue( value );
                if( value < 40 ){
                    threadsBar.setString( "Computer should run nominally" );
                    threadsBar.setForeground( Color.GREEN );
                }else if( value < 60 ){
                    threadsBar.setString( "Possibility of small slowdowns" );
                    threadsBar.setForeground( Color.YELLOW );
                }else if( value <= 80 ){
                    threadsBar.setString( "Other tasks affected slightly" );
                    threadsBar.setForeground( Color.ORANGE );
                }else{
                    threadsBar.setString( "Other tasks affected significantly" );
                    threadsBar.setForeground( Color.RED );
                }
                    
            }
    
            frame.setVisible( false );
        }
    
        protected abstract int getValue();
        protected abstract boolean keepRunning();
    }

}
