package frontend;

import java.awt.Dimension;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import general.Main;

public class FileChooser extends JFrame{
    
    JFileChooser fChooser = new JFileChooser();;

    public void main(){
        //INITIALIZATION CODE
        Main.getFrame().setVisible( false );
        setTitle( "File Selection" );
        setSize( new Dimension( 600, 450 ) );
        JPanel selectPanel = new JPanel();
        selectPanel.setVisible( true );
        add( selectPanel );
        Image icon = Main.getToolkit().getImage( "Resources\\FileOpen.png" );
        setIconImage( icon );
        setVisible( true );

    
        //CLOSE OPTIONS
        setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                setVisible( false );
                Main.getFrame().setVisible( true );
                return;
            }
        });

        //CREATES FILE CHOOSER
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Files(.xlsx); .pdf", "xlsx", "pdf");   //specifies types of files that are ok
        fChooser.setFileFilter(filter); 
        fChooser.setPreferredSize( new Dimension( 600, 400 ));                                                     //formats file chooser
    
        //FINALIZATION CODE
        selectPanel.add( fChooser );
    }

    protected void setListener( FileListener listener ){
        fChooser.addActionListener( e -> {
        setVisible( false ); listener.actionPerformed(e);} );
    }

    public interface FileListener extends ActionListener{
        @Override
        default public void actionPerformed(ActionEvent e) {    
            String str = new String();

            int startIndex = e.toString().indexOf( "returnValue=" ) + 12;
            int endIndex = e.toString().indexOf( ",selectedFile=" );
            String returnValue = e.toString().substring( startIndex, endIndex );
            if( returnValue.equals( "CANCEL_OPTION" ) ){
                str = "";
            }else if( returnValue.equals( "APPROVE_OPTION" ) ){
                startIndex = e.toString().indexOf( "selectedFile=" ) + 13;
                endIndex = e.toString().indexOf( ",useFileHiding=" );
                str = e.toString().substring( startIndex, endIndex );
            }
            actionPerformed( str );
        }
        
        public abstract void actionPerformed( String str );
    }

}