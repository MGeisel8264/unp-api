package add;

import javax.swing.*;
import general.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Add extends UPNMenu{

    //Private variables for frontend
    private PartCategories cBox;
    private int prevIndex = 0;                                                                                             //last index catBox was
    private String[] otherStrings;

    private JTextField upnField; 
    private JTextField partNumbField ;
    private JTextField descField;

    public Add(){
        super( "ADD NEW PART", "ADD" );
    }

    /**
     * Main method
     */
    @Override
    protected void centerPanel(){

        //String array
        otherStrings = new String[7];

        for( int i = 0; i < otherStrings.length; i++ ){
            otherStrings[i] = new String( "" );
        }

        //LAYOUT CREATION
        GridLayout upnLayout = new GridLayout( 3, 2 ); upnLayout.setHgap( 50 ); upnLayout.setVgap( 15 );           //Layout for the upn panel
        GridLayout fieldLayout = new GridLayout( 3, 2 ); fieldLayout.setHgap( 20 ); fieldLayout.setVgap( 10 );

        //PANEL CREATION
        JPanel centerPanel = new JPanel( new GridLayout( 2, 1 )); centerPanel.setBackground( Color.BLACK );         //creates the center panel
        JPanel upnGenerate = new JPanel( upnLayout ); upnGenerate.setBackground( Color.BLACK );                     //top panel: Get next part # and write new part #
        JPanel fieldPanel = new JPanel( fieldLayout ); fieldPanel.setBackground( Color.BLACK );                 

        //LABEL CREATION
        TxtLabel generateLabel = new TxtLabel( "Generate Part Number" );
        TxtLabel checkLabel = new TxtLabel( "Enter Custom Part Number" );
        TxtLabel upnLabel = new TxtLabel( "UPN" );
        TxtLabel descLabel = new TxtLabel( "Enter Description" );

        //TEXT AREA CREATION
        partNumbField = new JTextField( );
        upnField = new JTextField(); upnField.setEditable( false  );
        descField = new JTextField(); 
        JTextField otherField = new JTextField();

        //BUTTON AND COMBO BOX CREATION
        JButton generateButton = new JButton( "GENERATE" );                                         //Generates a part
        JButton checkButton = new JButton( "CHECK " );                                              //Checks the part
        cBox = new PartCategories();                                                 //Selects the prefix
        cBox.removeItemAt(99);                                                        //Removes special
        JComboBox<String> catBox = new JComboBox<String>( Arrays.copyOfRange( Main.ATTRIBUTE_NAMES, 2, Main.ATTRIBUTE_NAMES.length ) );

        //ADDS COMPONENTS TO UPN PANEL
        upnGenerate.setBorder( BorderFactory.createEmptyBorder( 10, 20, 30, 20) ); 
        upnGenerate.add( generateLabel ); upnGenerate.add( checkLabel );                   //GET NEXT              CUSTOM #   
        upnGenerate.add( cBox ); upnGenerate.add( partNumbField );                           //*selection box*       *text area*
        upnGenerate.add( generateButton ); upnGenerate.add( checkButton );             //(generate)            (check)
                                                                                                                    
        //ADDS COMPONENTS TO FIELD PANEL
        fieldPanel.setBorder( BorderFactory.createEmptyBorder( 10, 20, 10, 20) );                                                             //Sets border
        fieldPanel.add( upnLabel ); fieldPanel.add( descLabel );                          //UPN                   Enter Description
        fieldPanel.add( upnField ); fieldPanel.add( descField );                          //*UPN*                 *text area*
        fieldPanel.add( catBox );   fieldPanel.add( otherField );                         //<category>            *category*

        //ADD ACTION LISTENERS
        generateButton.addActionListener( e -> getNextPart() );
        checkButton.addActionListener( e -> checkPartNumb() );
        catBox.addActionListener( e -> {
            otherStrings[prevIndex] = descField.getText();
            descField.setText( otherStrings[catBox.getSelectedIndex()]);
            prevIndex = catBox.getSelectedIndex();
        });

        //ADD OTHER PANELS TO CENTER
        centerPanel.add( upnGenerate );
        centerPanel.add( fieldPanel );

        add( centerPanel, BorderLayout.CENTER );
    }

    /**
     * Sets upnField to the next part in the cBox index
     */
    private void getNextPart(){
        ArrayList<Part> partsList = new ArrayList<Part>();                                          //List of the parts
        try{
            partsList = Main.getDatabase().getList( cBox.getSelectedIndex() + 1 );                  //Sets partsList to the list of the parts in the selected array
        }catch( Exception ex ){ 
            JOptionPane.showMessageDialog( null, "ERROR: FAILED FETCH", "Error Message", JOptionPane.ERROR_MESSAGE );       //Failed fetch error message
        }

        Part lastPart = partsList.get( partsList.size() - 1 );                                      //Finds whatever the last part is
        int addNumb = Integer.parseInt( lastPart.getUPN().substring( lastPart.getUPN().indexOf("-")+1, lastPart.getUPN().indexOf("-") + 5) ) + 1;                   //Adds 1 to the last parts index

        //Runs through database adding until the part is not in the database
        while( Main.getDatabase().includes( cBox.getSelectedIndex() + 1, cBox.getSelectedIndex() + 1 + (( cBox.getSelectedIndex() + 1 > 10 )? "" : "0")  + "-" + addNumb) ){
            addNumb++;                                                                               
        }

        upnField.setText( cBox.getSelectedItem() + "-" + addNumb );             //Sets the upnField's text
    }

    /**
     * Checks the part number in partNumbField and sees if it is unique
     */
    private void checkPartNumb(){
        try{
            if( !Main.getDatabase().includes( Part.getPrefixOf( partNumbField.getText() ), partNumbField.getText() )){             //Checks if the part is in the database
                upnField.setText( partNumbField.getText() );                        //Sets the upnField to partNumbField if partNumbField is unique
            }else{
                JOptionPane.showMessageDialog( null, "ERROR: UPN NOT UNIQUE", "Error Message", JOptionPane.ERROR_MESSAGE );                           //Error Message
            }
        }catch( Exception ex ){                                                                                             //Error message
            JOptionPane.showMessageDialog( null, "ERROR: FAILED CHECK", "Error Message", JOptionPane.ERROR_MESSAGE );
        }
    }

    /**
     * This does the work of adding the part
     */
    @Override
    protected void bottomButton(){
        if( upnField.getText().equals("") || descField.getText().equals("") ){                                      //Makes sure there is a upn and description
            JOptionPane.showMessageDialog( null, "ERROR: UPN OR DESCRIPTION EMPTY", "Error Message", JOptionPane.ERROR_MESSAGE );       //Error message
            return;
        }

        try{
            Part part = new Part( "", "" );                                                                         //Generates new blank part
            String[] textAreaStrs = new String[7];
            textAreaStrs[0] = upnField.getText(); textAreaStrs[1] = descField.getText();                            //Sets first two values to teh fields
            for( int i = 2; i < textAreaStrs.length; i++ ){ textAreaStrs[i] = otherStrings[i]; }                    //Sets other attributes
            part.setAsArray( textAreaStrs );                                                                        //sets part
            
            Main.getDatabase().addPart(part);                                                                       //adds part

        }catch( IllegalArgumentException ex ){
            JOptionPane.showMessageDialog( null, "ERROR: " + ex.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE );
        }

        cancelButton();
    }
}
