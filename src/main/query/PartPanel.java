package query;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import general.*;

public final class PartPanel extends UPNMenu{

    private Part selectPart;
    private ThisField[] areas;

    public PartPanel( Part selectPart ){
        super( "U. P. N. " + selectPart.getUPN(), "RESET TEXT" );
        this.selectPart = selectPart;
    }

    private class ThisField extends JTextField{
        public ThisField( String text){
            super( text );
            setForeground( Color.WHITE );
            setBackground( Color.BLACK );
            setBorder( BorderFactory.createEmptyBorder());
            setEditable( true );
        }
    }
    
    @Override
    public void centerPanel(){
        
        //LAYOUT CREATION
        GridBagLayout gridBagLayout = new GridBagLayout(); 
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1.0; gbc.weighty = 1.0; gbc.fill = GridBagConstraints.HORIZONTAL;

        //PANEL CREATION
        JPanel mainPanel = new JPanel( gridBagLayout  ); mainPanel.setBackground( Color.BLACK ); 
            mainPanel.setBorder( BorderFactory.createEmptyBorder( 20, 20, 0, 10 ));

        //TEXT AREA CREATION
        ThisField[] partAreas = { new ThisField( selectPart.getDescription() ), new ThisField( selectPart.getDescription2() ),
            new ThisField( selectPart.getVendor() ), new ThisField( selectPart.getVPN() ), new ThisField( selectPart.getInitialsDate() ),
            new ThisField( selectPart.getUsedOn() ) };

        areas = partAreas;

        //LABEL CREATION
        TxtLabel[] labels = { new TxtLabel( "Description: " ), new TxtLabel( "Desc. 2: " ), new TxtLabel( "Vendor: " ),
         new TxtLabel( "VPN: " ), new TxtLabel( "Initials & Date: " ), new TxtLabel( "Used On: ") };


        //FONT CREATION & SETTING
        Font font = new Font("Courier", Font.BOLD,16); areas[0].setFont( font );
        labels[0].setFont( font );

        //ADDS COMPONENTS TO MAIN PANEL
        //Buffers galore
        for( int i = 0; i < 7; i++ ){
            gbc.gridx = i; gbc.gridy = 0;
            mainPanel.add(new JLabel(""),gbc); 
        }

        for( int i = 0; i < 6; i++ ){
            gbc.gridx = 0; gbc.gridy = i; gbc.gridwidth = 1;
            mainPanel.add( labels[i], gbc ); 

            gbc.gridx = 1; gbc.gridy = i; gbc.gridwidth = 6;
            mainPanel.add( areas[i], gbc );
        }

        add( mainPanel, BorderLayout.CENTER );
    }

    @Override
    protected void bottomPanel(){
        FlowLayout flowLayout = new FlowLayout(); flowLayout.setHgap(20);
        JPanel bottomPanel = new JPanel( flowLayout ); bottomPanel.setBackground( new Color( 6, 24, 54) );

        //BUTTONS
        JButton customButton = new JButton( bottomBtnText );
        JButton editButton = new JButton( "EDIT PART" );
        JButton deleteButton = new JButton( "DELETE PART" );
        JButton cancelButton = new JButton( "CANCEL" );
        bottomPanel.add( customButton ); bottomPanel.add( editButton );
        bottomPanel.add( deleteButton ); bottomPanel.add( cancelButton );

        add( bottomPanel, BorderLayout.SOUTH );

        customButton.addActionListener( e -> bottomButton() );
        editButton.addActionListener( new EditListener() );
        deleteButton.addActionListener( new DeleteListener() );
        cancelButton.addActionListener( e -> {
            setVisible( false );
            PartSelection selection = new PartSelection();
            selection.main();
        } );

    }

    private class EditListener implements ActionListener, PasswordProtected{

        @Override
        public void actionPerformed( ActionEvent e ){
            if( !checkPassword( "Enter Password to edit part" ) ){
                return;
            }

            Part newPart = new Part( selectPart.getUPN(), selectPart.getDescription() );
            newPart.setDescription2( areas[0].getText() ); newPart.setInitialsDate( areas[1].getText() );
            newPart.setVendor( areas[2].getText() ); newPart.setVPN( areas[3].getText() );
            newPart.setInitialsDate( areas[4].getText() ); newPart.setUsedOn( areas[5].getText() );

            Main.getDatabase().deletePart( selectPart );
            Main.getDatabase().addPart( newPart );

            setVisible( false );
            cancelButton();
        }

    }

    private class DeleteListener implements ActionListener, PasswordProtected{

        @Override
        public void actionPerformed( ActionEvent e ){
            if( !checkPassword( "Enter Password to delete part" ) ){
                return;
            }

            Main.getDatabase().deletePart( selectPart );
            setVisible( false );
            cancelButton();
        }   
    }

    @Override
    public void bottomButton(){
        areas[0].setText( selectPart.getDescription() ); areas[1].setText( selectPart.getDescription2() ); areas[2].setText( selectPart.getVendor() );
        areas[3].setText( selectPart.getVPN() ); areas[4].setText( selectPart.getInitialsDate() ); areas[5].setText( selectPart.getUsedOn() );
    }

}
