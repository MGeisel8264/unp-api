package query;

import general.*;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import java.awt.*;

public class Query extends UPNMenu{

    private PartCategories cBox = new PartCategories();
    private JTextPane upnArea;
    private JTextPane descArea;
    private boolean showObsolete = false;

    public Query(){
        super( "FIND EXISTING PART", "SEARCH/EDIT" );
    }

    @Override
    protected void centerPanel(){

        //LAYOUT CREATION
        GridLayout centralLayout = new GridLayout( 1, 2 ); centralLayout.setHgap( 20 ); 
        GridLayout bottomLayout = new GridLayout( 1, 2 ); bottomLayout.setHgap( 30 );

        //PANEL CREATION
        JPanel centralPanel = new JPanel( centralLayout ); centralPanel.setBackground( Color.BLACK );
        JPanel bottomPanel = new JPanel( bottomLayout );   bottomPanel.setBackground( Color.BLACK );
        JPanel mainPanel = new JPanel( new GridLayout( 2, 1 ) ); mainPanel.setBackground( Color.BLACK );

        //TEXT AREA CREATION
        upnArea = new JTextPane(); upnArea.setEditable( false ); 
            upnArea.setBackground( Color.BLACK);   //Left area- has the upns
        descArea = new JTextPane(); descArea.setEditable( false );
            descArea.setBackground( Color.BLACK); //Right area- has descriptions

        //SCROLL BAR CREATION- creates two scroll bar objects that can be used later
        JScrollPane upnScrollPane = new JScrollPane( upnArea); 
            upnScrollPane.setBorder( BorderFactory.createLineBorder( Color.DARK_GRAY) );
            upnScrollPane.setVerticalScrollBarPolicy( JScrollPane.VERTICAL_SCROLLBAR_ALWAYS );
        JScrollPane descScrollPane = new JScrollPane( descArea );
            descScrollPane.setBorder( BorderFactory.createLineBorder( Color.DARK_GRAY) );
            descScrollPane.setVerticalScrollBarPolicy( JScrollPane.VERTICAL_SCROLLBAR_ALWAYS );
        
        //LINK THE TWO SCROLL BARS TOGETHER
        upnScrollPane.getVerticalScrollBar().addAdjustmentListener
        ( event -> linkScrollBars( descScrollPane, upnScrollPane ) );
            
        descScrollPane.getVerticalScrollBar().addAdjustmentListener
        ( event -> linkScrollBars( upnScrollPane, descScrollPane ) );

        //CREATE LIST
        for( int i = 0; i < Main.getDatabase().getList( cBox.getSelectedIndex() + 1 ).size(); i++ ){
            addPartToPanel( i );
        }

        //ADDS ACTION LISTENER TO CBOX
        cBox.addActionListener( e -> {
            upnArea.setText( "" );
            descArea.setText( "" );
                
            for( int i = 0; i < Main.getDatabase().getList( cBox.getSelectedIndex() + 1 ).size(); i++ ){
                addPartToPanel( i );
            }
            repaint();
        });

        centralPanel.setBorder( BorderFactory.createEmptyBorder( 10, 10, 10, 10) );
        centralPanel.add( upnScrollPane ); centralPanel.add( descScrollPane );

        add( centralPanel, BorderLayout.CENTER );

    }

    private void addPartToPanel( int i ){
        Part thisPart = Main.getDatabase().getList( cBox.getSelectedIndex() + 1 ).get( i );

        StyledDocument upnDoc = upnArea.getStyledDocument();
        StyledDocument descDoc = descArea.getStyledDocument();

        Style upnStyle = upnArea.addStyle( "", null );
        Style descStyle = upnArea.addStyle( "", null );

        if( thisPart.isObsolete() ){
            if( !showObsolete ){
                return;
            }
            StyleConstants.setForeground( upnStyle, Color.red );
            StyleConstants.setForeground( descStyle, Color.red );
        }else{
            StyleConstants.setForeground( upnStyle, Color.blue );
            StyleConstants.setForeground( descStyle, Color.blue);
        }

        try{
            upnDoc.insertString( upnDoc.getLength(), thisPart.getUPN() + "\n", upnStyle );
            descDoc.insertString( descDoc.getLength(), thisPart.getDescription() + "\n", descStyle );
        }catch( BadLocationException e ){
            e.printStackTrace();
        }
    }

    private void linkScrollBars( JScrollPane scrollPane1, JScrollPane scrollPane2 ){
        JScrollBar scrollBar1 = scrollPane1.getVerticalScrollBar();                  //instantates the scroll bar
        scrollBar1.setValue( scrollPane2.getVerticalScrollBar().getValue() );        //sets the scroll bar value to the other bar
        scrollPane1.setVerticalScrollBar(  scrollBar1 );                     //adds the scroll bar
    }

    @Override
    protected void bottomPanel(){
        GridLayout gLayout = new GridLayout( 2, 3 ); gLayout.setHgap(20); gLayout.setVgap( 10 );
        JPanel bottomPanel = new JPanel( gLayout ); bottomPanel.setBackground( new Color( 6, 24, 54) );
        bottomPanel.setBorder( BorderFactory.createEmptyBorder( 10, 10, 10, 10 ) );
        JButton customButton = new JButton( bottomBtnText );
        JButton cancelButton = new JButton( "CANCEL" );
        TxtLabel selectLabel = new TxtLabel( "SELECT PREFIX" );
        JCheckBox showObs = new JCheckBox( "Show Obsolete Parts" ); showObs.setBackground( new Color( 6, 24, 54) ); showObs.setForeground( Color.WHITE );
        bottomPanel.add( selectLabel ); bottomPanel.add( new JLabel() ); bottomPanel.add( showObs );
        bottomPanel.add( cBox ); bottomPanel.add( customButton ); bottomPanel.add( cancelButton );
        add( bottomPanel, BorderLayout.SOUTH );

        showObs.addActionListener( e -> {
            showObsolete = !showObsolete;
            upnArea.setText( "" ); descArea.setText( "" );
            for( int i = 0; i < Main.getDatabase().getList( cBox.getSelectedIndex() + 1 ).size(); i++ ){
                addPartToPanel( i );
        }   } );
        customButton.addActionListener( e -> bottomButton() );
        cancelButton.addActionListener( e -> cancelButton() );
    }

    @Override
    protected void bottomButton(){
        closePanel();
        PartSelection thisPartSelection = new PartSelection();
        thisPartSelection.main();
    }

}
