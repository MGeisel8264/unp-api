package query;

import general.*;
import javax.swing.*;
import javax.swing.text.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public final class PartSelection extends UPNMenu{

    public PartSelection(){
        super( "SELECT PART", "SELECT" );
        
    }

    private int pastChoice = 0;             //stores the past choice of the combobox.
    private PartCategories cBox = new PartCategories();         //Combo box of selected index
    JComboBox<String> catBox = new JComboBox<String>();
    private SearchTextArea descriptionArea = new SearchTextArea();
    private SearchTextArea otherAttrArea = new SearchTextArea();
    private String[] categoryInput = { "", "", "", "", "" }; 
    private Part chosenPart;
    private JCheckBox showObs = new JCheckBox( "Show Obsolete Searches" );
    
    @Override
    protected void centerPanel(){

        //LAYOUT CREATION
        GridLayout topLayout = new GridLayout( 1, 4 ); topLayout.setHgap( 5 ); 
        GridLayout midLayout = new GridLayout( 1, 2);  midLayout.setHgap( 20 );
        GridLayout bottomLayout = new GridLayout( 1, 4 );  bottomLayout.setHgap( 20 ); 

        //PANEL CREATION
        JPanel centerPanel = new JPanel( new BorderLayout() ); centerPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ));
        centerPanel.setBackground( Color.BLACK );
        JPanel topPanel = new JPanel( topLayout ); topPanel.setBorder( BorderFactory.createEmptyBorder( 5, 50, 5, 50 ));
        topPanel.setBackground( Color.BLACK );
        JPanel midPanel = new JPanel( midLayout ); midPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ));
        midPanel.setBackground( Color.BLACK );
        JPanel bottomPanel = new JPanel( bottomLayout ); bottomPanel.setBorder( BorderFactory.createEmptyBorder( 5, 20, 5, 20 ));
        bottomPanel.setBackground( Color.BLACK );
        
        //LABEL CREATION
        TxtLabel prefixLabel = new TxtLabel( "Prefix: " ); 
        TxtLabel descLabel = new TxtLabel( "v Description v" ); 
        TxtLabel settingsLabel = new TxtLabel( "Edit Settings: " );        

        //OTHER USEFUL OBJECT CREATION
        cBox.addItem( "Unknown Prefix..." );
        catBox = new JComboBox<String>( Arrays.copyOfRange( Main.ATTRIBUTE_NAMES, 2, 7 ) );           //This displays the categories

        //BUTTON CREATION
        JButton searchBtn = new JButton( "SEARCH" );
        JButton settingsBtn = new JButton( "SELECT" );
        JButton infoButton = new JButton( "INFO" );

        //TEXT AREA CREATION
        descriptionArea.setBackground( Color.BLACK ); descriptionArea.setForeground( Color.cyan );

        catBox.addActionListener( (event)->{
            categoryInput[pastChoice] = otherAttrArea.getText();
            otherAttrArea.setText( categoryInput[catBox.getSelectedIndex()]);
            pastChoice = catBox.getSelectedIndex();
        });

        //INFO PANEL
        infoButton.addActionListener( e -> JOptionPane.showMessageDialog( null, "Seperate search queries with any operator or a new line( default operator )" +
        "\n\nUse <operator> to edit the type of search (ex. <and> camera )" + 
        "\noperator: <and>, <or>, <not>, <open>, <close>, <> (Exact)" +
        "\n\n<and>: true if true <and> true" + 
        "\n<or>: true if true <or> false" + 
        "\n<not>: true if true <not> false (false if true <not> true )" +
        "\n<open> & <close>: parenthesis- true <or> false <and> false = false; true <or><open> false <and> false <closed> = true" +
        "\n MAKE SURE EACH <open> HAS A <close> AND THAT THERE ARE NOT MORE <close> THAN <open>" + 
        "\n<>: The string after <> is exactly what will be searched( except capitilization ). A second <> will not do anything."
        , "Info", JOptionPane.INFORMATION_MESSAGE ) );

        settingsBtn.addActionListener( e -> {
            boolean[] prevOptions = { showObs.isSelected() };
            Object[] objects = { showObs };
            int i = JOptionPane.showConfirmDialog( null, objects, "Settings", JOptionPane.DEFAULT_OPTION );

            if( i == -1 ){
                showObs.setSelected( prevOptions[0] );
            }
            
        });
        searchBtn.addActionListener( event -> {searchForPart();});
        
        topPanel.add( descLabel ); topPanel.add( prefixLabel ); topPanel.add( cBox ); topPanel.add( catBox ); 
        midPanel.add( descriptionArea ); midPanel.add( otherAttrArea );
        bottomPanel.add( infoButton ); bottomPanel.add( searchBtn );
        bottomPanel.add( settingsLabel ); bottomPanel.add( settingsBtn );
         
        centerPanel.add( topPanel, BorderLayout.NORTH );
        centerPanel.add( midPanel, BorderLayout.CENTER );
        centerPanel.add( bottomPanel, BorderLayout.SOUTH );         

        add( centerPanel, BorderLayout.CENTER );
    }

    private void searchForPart(){
        
        ArrayList<Part> acceptableList = new ArrayList<Part>();                 //stores all acceptable parts


        if( !cBox.getSelectedItem().equals( "Unknown Prefix..." ) ){
           acceptableList = Main.getDatabase().getList( cBox.getSelectedIndex()+1, showObs.isSelected() );       //sets acceptable parts to parts if known prefix
        }else{
            for( int i = 0; i < Main.getDatabase().size(); i++ ){
                acceptableList.addAll( Main.getDatabase().getList( i, showObs.isSelected() ) );                      //acceptable parts are all parts if unknown
            }
        }

        //checks if something hasn't been updated
        categoryInput[ catBox.getSelectedIndex() ] = otherAttrArea.getText();

        //CREATES ARRAY WITH ALL QUERIES
        String[] catInpt = new String[6];
        catInpt[0] = descriptionArea.getText();
        for( int i = 1; i < 6; i++ ){
            catInpt[i] = categoryInput[i - 1];
        }

        acceptableList = getApplicableParts( acceptableList, catInpt );

        //Prints out the Selection Box
        ArrayList<String> comboBoxArrList = new ArrayList<String>();

        for( int i = 0; i < acceptableList.size(); i++ ){
            comboBoxArrList.add( acceptableList.get(i).getUPN() + "; " + acceptableList.get(i).getDescription() );
        }

        String[] comboBoxArr = new String[comboBoxArrList.size()];
        for( int i = 0; i < comboBoxArr.length; i++ ){
            comboBoxArr[i] = comboBoxArrList.get(i);
        }

        JComboBox<String> optionCBox = new JComboBox<String>( comboBoxArr );

        if( comboBoxArr.length == 0 ){
            JOptionPane.showMessageDialog( null, "No Part Satisfied Query", "Error", JOptionPane.ERROR_MESSAGE );
        }else{
            JOptionPane.showMessageDialog( null, optionCBox, "Select Part", JOptionPane.QUESTION_MESSAGE );
            chosenPart = acceptableList.get( optionCBox.getSelectedIndex() );
        }
        
    
    }
    private static enum Operator{ START, AND, OR, NOT, OPEN, CLOSE, EXACT };
    private static Operator currentOperator = Operator.START;
    private static Operator pastOperator = Operator.START;
    private static int index = 0;
    private static ArrayList<Boolean> prevBoolList = new ArrayList<Boolean>();
    private static ArrayList<Operator> openingOperators = new ArrayList<Operator>();
    //TODO: THIS NEEDS IMPROVEMENT
    private static ArrayList<Part> getApplicableParts( ArrayList<Part> parts, String[] conditions ){

        ArrayList<Part> returnParts = new ArrayList<Part>( parts );

        for( int p = 0; p < returnParts.size(); p++ ){
            currentOperator = Operator.START; 
            index = 0;

            prevBoolList = new ArrayList<Boolean>();
            openingOperators = new ArrayList<Operator>();
            prevBoolList.add( true );
            for( int c = 0; c < conditions.length; c++ ){
                //checks if it is empty or just white spaces
                if( conditions[c].trim().equals( "" ) ){
                    continue;
                }
                conditions[c] = conditions[c].trim();
                String[] lineList = conditions[c].split( "\n" );
                conditions[c].replaceAll( "\n", "" );
                for( int i = 0; i < lineList.length - 1; i++ ){
                    if( !( lineList[i].trim().charAt( lineList[i].trim().length() - 1 ) == '>' )
                    && !( lineList[i+1].trim().charAt( lineList[i+1].trim().length() - 1 ) == '<' )  ){
                        int index = conditions[c].indexOf( lineList[i] ) + lineList[i].length();
                        conditions[c] = conditions[c].substring( 0, index ) + "<and>" + conditions[c].substring( index );
                    }
                }

                String[] conditionList = conditions[c].split( "<" );

                String compareString = new String();
                switch( c ){
                    case 0: compareString = returnParts.get( p ).getDescription(); break;
                    case 1: compareString = returnParts.get( p ).getDescription2(); break;
                    case 2: compareString = returnParts.get( p ).getVendor(); break;
                    case 3: compareString = returnParts.get( p ).getVPN(); break;
                    case 4: compareString = returnParts.get( p ).getInitialsDate(); break;
                    case 5: compareString = returnParts.get( p ).getUsedOn(); break;
                }
                compareString = compareString.toLowerCase();
                compareString = compareString.trim();

                for( int k = 0; k < conditionList.length; k++ ){
                    String compareToStr = new String();

                    if( conditionList[k].length() == 0 ){ continue; }

                    if( conditionList[k].charAt( 0 ) == '>' ){
                        if( currentOperator == Operator.EXACT ){
                            continue; 
                        }else{
                            pastOperator = currentOperator;
                            currentOperator = Operator.EXACT;
                        }
                    }else{
                        conditionList[k] = conditionList[k].trim();
                    }

                    conditionList[k] = conditionList[k].toLowerCase();

                    if( !conditionList[k].contains( ">" ) ){
                        if( c != 0 ){ 
                            currentOperator = Operator.AND;
                        }else{
                            currentOperator = Operator.START;
                        }
                        compareToStr = conditionList[k];
                    }else{
                        String[] opAndCondition = conditionList[k].split( ">" );                     //index 0 is the condition and index 1 is the condition
                        if( opAndCondition[0].equals( "and" ) ){
                            currentOperator = Operator.AND;
                        }else if( opAndCondition[0].equals( "or" ) ){
                            currentOperator = Operator.OR;
                        }else if( opAndCondition[0].equals( "not" ) ){
                            currentOperator = Operator.NOT;
                        }else if( opAndCondition[0].equals( "open" )){
                            openingOperators.add( currentOperator );
                            currentOperator = Operator.OPEN;
                        }else if( opAndCondition[0].equals( "close" ) ){
                            currentOperator = Operator.CLOSE;
                        }
                        try{
                            if( currentOperator != Operator.EXACT ){ 
                                compareToStr = opAndCondition[1].trim(); 
                            }else{
                                compareToStr = opAndCondition[1];
                            }
                        }catch( Exception e ){ if( k < conditionList.length - 1 ){ continue; } }
                    }

                    checkOperator( compareString, compareToStr, prevBoolList.get( index ) );
                    
                }
               
                
            }
            if( !prevBoolList.get(0) ){ returnParts.remove( p ); p--;}
        }
        return returnParts;

    }

    private static void checkOperator( String compareString, String compareToStr, boolean bool1 ){
        if( currentOperator == Operator.START ){
            prevBoolList.set( index, compareString.contains( compareToStr ) );
        }else if( currentOperator == Operator.AND ){
            prevBoolList.set( index, (  bool1 && compareString.contains( compareToStr ) ) );
        }else if( currentOperator == Operator.OR ){
            prevBoolList.set( index, ( bool1 || compareString.contains( compareToStr ) ) );
        }else if( currentOperator == Operator.NOT ){
            prevBoolList.set( index, ( bool1 && !compareString.contains( compareToStr ) ) );
        }else if( currentOperator == Operator.OPEN ){
            index++;
            prevBoolList.add( compareString.contains( compareToStr ) );
        }else if( currentOperator == Operator.CLOSE ){
            index--;
            currentOperator = openingOperators.get( index );
            openingOperators.remove( index );
            checkOperator( compareString, compareToStr, prevBoolList.get( index + 1 ), prevBoolList.get( index ) );
            prevBoolList.remove( index + 1 );
        }else if( currentOperator == Operator.EXACT ){
            currentOperator = pastOperator;
            checkOperator( compareString, compareToStr, bool1 );
            currentOperator = Operator.EXACT;
        }
    }

    private static void checkOperator( String compareString, String compareToStr, boolean bool1, boolean bool2 ){
        if( currentOperator == Operator.START ){
            prevBoolList.set( index, compareString.contains( compareToStr ) );
        }else if( currentOperator == Operator.AND ){
            prevBoolList.set( index, (  bool1 && bool2 ) );
        }else if( currentOperator == Operator.OR ){
            prevBoolList.set( index, ( bool1 || bool2 ) );
        }else if( currentOperator == Operator.NOT ){
            prevBoolList.set( index, ( bool1 && !bool2 ) );
        }else if( currentOperator == Operator.OPEN ){
            index++;
            prevBoolList.add( compareString.contains( compareToStr ) );
        }else if( currentOperator == Operator.CLOSE ){
            index--;
            currentOperator = openingOperators.get( index );
            openingOperators.remove( index );
            checkOperator( compareString, compareToStr, prevBoolList.get( index + 1 ), prevBoolList.get( index ) );
            prevBoolList.remove( index + 1 );
        }else if( currentOperator == Operator.EXACT ){
            currentOperator = pastOperator;
            checkOperator( compareString, compareToStr, bool1 );
            currentOperator = Operator.EXACT;
        }
    }

    @Override
    public void bottomButton(){
        if( chosenPart == null ){
            JOptionPane.showMessageDialog( null, "ERROR: No Part Selected", "Error Message", JOptionPane.ERROR_MESSAGE );
        }else{
            setVisible( false );
            PartPanel thisPartPanel = new PartPanel( chosenPart );
            thisPartPanel.main();
        }
    }

    /**
     * The text area which makes searching fancy AND EASY :O
     */
    private class SearchTextArea extends JTextPane{

        public SearchTextArea(){
            setBackground( Color.BLACK ); setForeground( Color.cyan );
            setBorder( BorderFactory.createLineBorder( Color.DARK_GRAY ) );
            setCaretColor( Color.WHITE);
            addKeyListener( new KeyListener(){
                @Override public void keyPressed( KeyEvent e) { };
                @Override public void keyTyped(KeyEvent e) {  };
                @Override public void keyReleased(KeyEvent e) {  int pos = getCaretPosition(); 
                    editColors( e.getKeyChar() );
                    try{
                        setCaretPosition( pos );
                    } catch( Exception ex ){}
                    };
            } );
        }

        public void editColors( char keyReleased ){
            StyledDocument currentDoc = getStyledDocument();
            Style currentStyle = addStyle( "", null );

            String text = getText();

            setText( "" );
            
            if( text.equals( "<" ) ){
                StyleConstants.setForeground( currentStyle , Color.RED );
                try{
                    currentDoc.insertString( currentDoc.getLength(), "<", currentStyle );
                    return;
                }catch( Exception e ){}
                
            }

            ArrayList<String> queries = new ArrayList<String>();
            for( int i = 0; i < text.split( "<" ).length; i++ ){
                queries.add( text.split( "<" )[i] );
            }

            try{
                if( text.charAt( text.length() - 1 ) == '<' ){
                    queries.add( new String() );
                }else if( text.equals( ">" )){
                    StyleConstants.setForeground( currentStyle , Color.RED ); 
                    currentDoc.insertString( currentDoc.getLength(), ">", currentStyle );
                    return;
                }
            }catch( Exception e ){}
            

            int opensToCloses = 0;

            for( int i = 0; i < queries.size(); i++ ){
                try{
                    if( queries.get( i ).substring( 0, 5 ).equals( "open>" ) ){
                        opensToCloses++;
                    }else if( queries.get( i ).substring( 0, 6 ).equals( "close>" ) ){
                        opensToCloses--;
                    }
                }catch( Exception e ){}

            }

            

            for( int i = 0; i < queries.size(); i++ ){
                try{
                    if( i == 0 && !queries.get( i ).contains( ">" )){
                        StyleConstants.setForeground( currentStyle , Color.CYAN );
                        currentDoc.insertString( currentDoc.getLength(), queries.get( i ), currentStyle );
                    }else if( queries.get( i ).length() - queries.get( i ).replace( ">","").length() != 1){
                        StyleConstants.setForeground( currentStyle , Color.RED );
                        currentDoc.insertString( currentDoc.getLength(), "<" + queries.get( i ), currentStyle );
                    }
                    //If it is an exact line query
                    else if( queries.get( i ).charAt( 0 ) == '>' ){
                        StyleConstants.setForeground( currentStyle , new Color( 100, 100, 255 ) );
                        currentDoc.insertString( currentDoc.getLength(), "<>", currentStyle );
                        StyleConstants.setForeground( currentStyle , Color.WHITE );
                        try{
                            currentDoc.insertString( currentDoc.getLength(), queries.get( i ).substring(1), currentStyle );
                        }catch( Exception e ){}
                    }

                    //and, or, not 
                    else if( queries.get( i ).substring( 0, 3 ).equals( "and" ) || queries.get( i ).substring( 0, 2 ).equals( "or" ) || queries.get( i ).substring( 0, 3 ).equals( "not" )){
                        String[] inputs = queries.get( i ).split( ">" );
                        StyleConstants.setForeground( currentStyle , new Color( 100, 100, 255 ) );
                        currentDoc.insertString( currentDoc.getLength(), "<" + inputs[0] + ">", currentStyle );
                        StyleConstants.setForeground( currentStyle , Color.CYAN );
                        currentDoc.insertString( currentDoc.getLength(), inputs[1], currentStyle );
                    }else if( queries.get( i ).substring( 0, 4 ).equals( "open" ) || queries.get( i ).substring( 0, 5 ).equals( "close" ) ){
                        int stopIndex = queries.get( i ).indexOf( ">" ) + 1;

                        if( opensToCloses == 0 ){ StyleConstants.setForeground( currentStyle , new Color( 100, 100, 255 ) ); }
                        else{ StyleConstants.setForeground( currentStyle , Color.RED ); }

                        currentDoc.insertString( currentDoc.getLength(), "<" + queries.get( i ).substring( 0, stopIndex ), currentStyle );

                        StyleConstants.setForeground( currentStyle , Color.CYAN );
                        try{
                            currentDoc.insertString( currentDoc.getLength(), queries.get( i ).substring(stopIndex), currentStyle );
                        }catch( Exception e ){}
                    }else{
                        StyleConstants.setForeground( currentStyle , Color.RED ); 
                        currentDoc.insertString( currentDoc.getLength(), "<" + queries.get( i ), currentStyle );

                    }

                    //if its a open or close
                    
                    
                    
                }catch( Exception e ){}
            }
        }

    }

}
